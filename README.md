# JsCad Browserify Test

![screenshot](screenshot.png)

This is a test to try johnwebbcole/jscad-utils and the new openjscad libraries.

## @jscad

JsCad is undergoing modernization into an npm installable set of libraries.
Currently using Browserify, you can start building against the new modules.

## jscad-utils

jscad-utils is a set of utilities for jscad. Originaly intended for the 1.x
jscad system, an es6 version is available at `@jwc/jscad-utils`.

## csg-viewer

Using `csg-viewer` forked from the official @jscad repo, a demo
was created using jscad-utils and the new @jscad libraries.

You can try out the live demo at https://johnwebbcole.gitlab.io/jscad-browserify-test
