/**
 * An attempt at using the browserify api... not used atm.
 */

var fs = require("fs");
var browserify = require("browserify");
browserify("./index.js")
  .transform("babelify", { presets: ["@babel/preset-env"] })
  .external("@jscad/csg")
  .external("@jscad/scad-api")
  .bundle()
  .pipe(fs.createWriteStream("bundle.js"));
