var { util, parts, array } = require("@jwc/jscad-utils");
const CSG = require("@jscad/csg").CSG;
console.log("index.js util", util, parts, array);
const { union } = require("@jscad/scad-api").booleanOps;

module.exports = function main() {
  // return parts.Cube([100, 100, 100]);

  return Clip(21);
};

function Clip(OD) {
  console.log("Nozzel", util);
  var pipe = parts.Cylinder(OD, 25, {
    resolution: CSG.defaultResolution2D
  });
  var thickness = util.nearest.over(3);
  var body = parts
    .Cylinder(OD + thickness, 25, {
      resolution: CSG.defaultResolution2D
    })
    .subtract([pipe]);
  return union([body, parts.Cube([25, 25, 25])])
    .subtract([
      parts
        .Cube([25 - thickness, 25 - thickness, 25])
        .translate([thickness / 2, thickness / 2, 0])
    ])
    .subtract(pipe)
    .intersect(
      parts.Cylinder(OD * 1.5, 25, {
        resolution: CSG.defaultResolution2D
      })
    );
}
